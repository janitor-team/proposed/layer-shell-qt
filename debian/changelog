layer-shell-qt (5.26.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.26.3).

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 08 Nov 2022 15:42:51 +0100

layer-shell-qt (5.26.2-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.26.1).
  * New upstream release (5.26.2).

 -- Aurélien COUDERC <coucouf@debian.org>  Thu, 27 Oct 2022 23:30:12 +0200

layer-shell-qt (5.26.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.26.0).
  * Release to unstable.

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 11 Oct 2022 15:44:01 +0200

layer-shell-qt (5.25.90-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.25.90).
  * Update build-deps and deps with the info from cmake.
  * Update symbols from build for 5.25.90.

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 25 Sep 2022 00:14:21 +0200

layer-shell-qt (5.25.5-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.25.5).

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 09 Sep 2022 23:20:48 +0200

layer-shell-qt (5.25.4-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.25.4).

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 02 Aug 2022 17:30:56 +0200

layer-shell-qt (5.25.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.25.2).
  * New upstream release (5.25.3).
  * Release to unstable

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 17 Jul 2022 15:26:30 +0200

layer-shell-qt (5.25.1-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.25.1).

 -- Patrick Franz <deltaone@debian.org>  Tue, 21 Jun 2022 21:44:41 +0200

layer-shell-qt (5.25.0-1) experimental; urgency=medium

  * New upstream release (5.25.0).
  * Bump Standards-Version to 4.6.1, no change required.

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 14 Jun 2022 21:26:12 +0200

layer-shell-qt (5.24.90-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.24.90).
  * Update symbols from build for 5.24.90.

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 20 May 2022 11:26:01 +0200

layer-shell-qt (5.24.5-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.24.5).

 -- Aurélien COUDERC <coucouf@debian.org>  Thu, 12 May 2022 21:39:18 +0200

layer-shell-qt (5.24.4-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.24.4).

 -- Aurélien COUDERC <coucouf@debian.org>  Wed, 30 Mar 2022 13:44:36 +0200

layer-shell-qt (5.24.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.24.3).
  * Added myself to the uploaders.

 -- Aurélien COUDERC <coucouf@debian.org>  Thu, 10 Mar 2022 07:45:58 +0100

layer-shell-qt (5.24.2-1) unstable; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.24.2).
  * Bump Standards-Version to 4.6.0 (no changes needed).
  * Re-export signing key without extra signatures.
  * Update d/copyright.
  * Update descriptions and sections.

 -- Patrick Franz <deltaone@debian.org>  Sat, 26 Feb 2022 21:58:59 +0100

layer-shell-qt (5.23.5-1) unstable; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.23.5).

 -- Patrick Franz <deltaone@debian.org>  Fri, 07 Jan 2022 15:44:21 +0100

layer-shell-qt (5.23.4-1) unstable; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.23.4).

 -- Patrick Franz <deltaone@debian.org>  Thu, 02 Dec 2021 20:24:45 +0100

layer-shell-qt (5.23.3-1) unstable; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.23.3).

 -- Norbert Preining <norbert@preining.info>  Wed, 10 Nov 2021 08:39:50 +0900

layer-shell-qt (5.23.2-1) unstable; urgency=medium

  [ Patrick Franz ]
  * Update upstream signing-key.
  * Add myself as uploader.

  [ Norbert Preining ]
  * New upstream release (5.23.1).
  * New upstream release (5.23.2).

 -- Norbert Preining <norbert@preining.info>  Wed, 03 Nov 2021 22:21:03 +0900

layer-shell-qt (5.23.0-1) unstable; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.23.0)

 -- Norbert Preining <norbert@preining.info>  Thu, 14 Oct 2021 20:13:15 +0900

layer-shell-qt (5.21.90-1) experimental; urgency=medium

  * First upload to Debian (Closes: #988577)

 -- Norbert Preining <norbert@preining.info>  Thu, 20 May 2021 18:31:09 +0900
